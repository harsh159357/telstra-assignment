package com.harshsharma.telstra.assignment.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import android.support.test.runner.lifecycle.Stage;

import com.harshsharma.telstra.assignment.R;
import com.harshsharma.telstra.assignment.activities.DashBoardActivity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Collection;

public class DashBoardTest {
    @Rule
    public ActivityTestRule<DashBoardActivity> rule =
            new ActivityTestRule<>(DashBoardActivity.class, true, false);

    private Activity activity;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testDashBoardActivity() {
        statActivity();
        Espresso.onView(ViewMatchers.withId(R.id.app_bar_layout)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withId(R.id.toolbar_title)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withId(R.id.menu_refresh)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withId(R.id.rv_facts)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
    }

    @Test
    public void testActivityIsDashBoardActivity() {
        statActivity();
        activity = getActivityInstance();
        Assert.assertTrue(activity instanceof DashBoardActivity);
    }

    @Test
    public void testActivityNotDestroyed() {
        statActivity();
        activity = getActivityInstance();
        Assert.assertFalse(activity.isDestroyed());
    }

    private void statActivity() {
        Intent intent = new Intent();
        activity = rule.launchActivity(intent);
        SystemClock.sleep(2000);
    }


    public static Activity getActivityInstance() {
        final Activity[] currentActivity = {null};
        try {
            InstrumentationRegistry.getInstrumentation().runOnMainSync(new Runnable() {
                public void run() {
                    Collection resumedActivities = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED);
                    if (resumedActivities.iterator().hasNext()) {
                        currentActivity[0] = (Activity) resumedActivities.iterator().next();
                    }
                }
            });
        } catch (Exception e) {

        }
        return currentActivity[0];
    }
}