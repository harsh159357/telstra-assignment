package com.harshsharma.telstra.assignment.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.SystemClock;
import android.support.annotation.RestrictTo;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.harshsharma.telstra.assignment.R;
import com.harshsharma.telstra.assignment.activities.DashBoardActivity;
import com.harshsharma.telstra.assignment.fragments.FactsFragment;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static android.support.test.espresso.action.ViewActions.click;

public class FactsFragmentTest {
    @Rule
    public ActivityTestRule<DashBoardActivity> activityTestRule =
            new ActivityTestRule<>(DashBoardActivity.class, true, false);

    private Activity activity;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testFactsClickInFragment() {
        statActivity();
        final FragmentManager fragmentManager = activityTestRule.getActivity().getSupportFragmentManager();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                launchFactsFragment(fragmentManager);
            }
        });
        SystemClock.sleep(100);
        Espresso.onView(withIndex(ViewMatchers.withId(R.id.networkImageView), 2)).perform(click());
    }

    @Test
    public void testFragmentCount() {
        statActivity();
        final FragmentManager fragmentManager = activityTestRule.getActivity().getSupportFragmentManager();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                launchFactsFragment(fragmentManager);

            }
        });
        SystemClock.sleep(100);
        Assert.assertTrue(fragmentManager.getBackStackEntryCount() > 0);
    }

    @Test
    public void testFragmentName() {
        statActivity();
        final FragmentManager fragmentManager = activityTestRule.getActivity().getSupportFragmentManager();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                launchFactsFragment(fragmentManager);

            }
        });
        SystemClock.sleep(100);
        Fragment fragment = getVisibleFragment(fragmentManager);
        Assert.assertEquals(((FactsFragment) fragment).getFragmentName(), "FactsFragment");
    }

    @Test
    public void testFragmentResumed() {
        statActivity();
        final FragmentManager fragmentManager = activityTestRule.getActivity().getSupportFragmentManager();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                launchFactsFragment(fragmentManager);

            }
        });
        SystemClock.sleep(100);
        Fragment fragment = getVisibleFragment(fragmentManager);
        Assert.assertTrue(((FactsFragment) fragment).isVisible());
    }


    private void launchFactsFragment(FragmentManager fragmentManager) {
        FactsFragment feedFragment = new FactsFragment();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.dash_board_content, feedFragment);
        fragmentTransaction.addToBackStack(FactsFragment.class.getName());
        fragmentTransaction.commit();
    }


    private void statActivity() {
        Intent intent = new Intent();
        activity = activityTestRule.launchActivity(intent);
        SystemClock.sleep(1000);
    }

    @RestrictTo(RestrictTo.Scope.LIBRARY_GROUP)
    public Fragment getVisibleFragment(FragmentManager fragmentManager) {
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }

    public static Matcher<View> withIndex(final Matcher<View> matcher, final int index) {
        return new TypeSafeMatcher<View>() {
            int currentIndex = 0;

            @Override
            public void describeTo(Description description) {
                description.appendText("with index: ");
                description.appendValue(index);
                matcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                return matcher.matches(view) && currentIndex++ == index;
            }
        };
    }

}
