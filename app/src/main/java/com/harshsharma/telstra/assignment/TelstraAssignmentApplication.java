package com.harshsharma.telstra.assignment;

import android.app.Application;

import com.harshsharma.telstra.assignment.interfaces.AppConstants;
import com.harshsharma.telstra.assignment.interfaces.retrofit.ApiInterface;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


//Main Entry Point for app also app Specifics things must be initialized here
public class TelstraAssignmentApplication extends Application implements AppConstants {

    private static TelstraAssignmentApplication telstraAssignmentApplication;

    private ApiInterface apiInterface;

    public TelstraAssignmentApplication() {
        telstraAssignmentApplication = this;
    }

    public static TelstraAssignmentApplication getInstance() {
        if (telstraAssignmentApplication == null) {
            telstraAssignmentApplication = new TelstraAssignmentApplication();
        }
        return telstraAssignmentApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initRetrofitApiInterface();
    }

    private void initRetrofitApiInterface() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES);
        OkHttpClient client = builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiEndPoint.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        apiInterface = retrofit.create(ApiInterface.class);
    }

    public ApiInterface getApiInterface() {
        return apiInterface;
    }
}
