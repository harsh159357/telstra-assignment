package com.harshsharma.telstra.assignment.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.harshsharma.telstra.assignment.R;
import com.harshsharma.telstra.assignment.interfaces.AppConstants;
import com.harshsharma.telstra.assignment.models.commons.EventObject;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Vector;

import butterknife.ButterKnife;


/*
        All activities in the app must extend this Activity common Tasks Like
        Event Bus and ButterKnife Initialization Done Here.
*/

public abstract class BaseActivity extends AppCompatActivity implements AppConstants {
    public static final int SHOW_TOAST = 0;
    private PauseHandler pauseHandler = new PauseHandler();
    private ProgressDialog progressDialog;

    /*
        This method needs to be implemented by every activity as it is layout supplier
        for the implementor
    */
    protected abstract
    @LayoutRes
    int getLayout();

    /*
        To Capture events fired by Event Bus following method needs to be implemented
    */
    @Subscribe
    public abstract void onEvent(EventObject eventObject);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        injectViews();
        EventBus.getDefault().register(this);
        initProgressDialog(getString(R.string.progress_text_loading));
    }

    // Initializing Progress Dialog to be used by whole app
    private void initProgressDialog(String msg) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(msg);
    }

    public void showProgress(String msg) {
        progressDialog.setMessage(msg);
        progressDialog.show();
    }

    public void dismissProgress() {
        progressDialog.dismiss();
    }

    private void injectViews() {
        ButterKnife.bind(this);
    }

    public void sendMessageToHandler(int what, int arg1, int arg2, Object response) {
        Message message = pauseHandler.obtainMessage();
        message.obj = response;
        message.what = what;
        message.arg1 = arg1;
        message.arg2 = arg2;
        pauseHandler.sendMessage(message);
    }

    @Override
    protected void onResume() {
        super.onResume();
        pauseHandler.setBaseActivity(this);
        pauseHandler.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        pauseHandler.pause();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        progressDialog.dismiss();
        super.onDestroy();
    }

    public boolean isActivityPaused() {
        return pauseHandler.isPaused();
    }

    public void processMessage(Message message) {
        switch (message.what) {
            case SHOW_TOAST:
                String msg = (String) message.obj;
                Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                break;
        }
    }


    public void showToast(String toastMessage) {
        sendMessageToHandler(SHOW_TOAST, -1, 1, toastMessage);
    }

    @SuppressLint("HandlerLeak")
    private class PauseHandler extends Handler {

        /**
         * Message Queue Buffer
         */
        final Vector<Message> messageQueueBuffer = new Vector<>();
        BaseActivity baseActivity;
        /**
         * Flag indicating the pause state
         */
        private boolean paused;

        /**
         * Resume the handler
         */
        final void resume() {
            paused = false;

            while (messageQueueBuffer.size() > 0) {
                final Message msg = messageQueueBuffer.elementAt(0);
                messageQueueBuffer.removeElementAt(0);
                sendMessage(msg);
            }
        }

        /**
         * Pause the handler
         */
        final void pause() {
            paused = true;
        }

        boolean isPaused() {
            return paused;
        }

        final void setBaseActivity(BaseActivity baseActivity) {
            this.baseActivity = baseActivity;
        }

        boolean storeMessage(Message message) {
            return true;
        }


        @Override
        final public void handleMessage(Message msg) {
            if (paused) {
                if (storeMessage(msg)) {
                    Message msgCopy = new Message();
                    msgCopy.copyFrom(msg);
                    messageQueueBuffer.add(msgCopy);
                }
            } else {
                processMessage(msg);
            }
        }
    }


}
