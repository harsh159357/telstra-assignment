package com.harshsharma.telstra.assignment.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.harshsharma.telstra.assignment.R;
import com.harshsharma.telstra.assignment.customview.TextViewCustom;
import com.harshsharma.telstra.assignment.fragments.BaseFragment;
import com.harshsharma.telstra.assignment.fragments.FactsFragment;
import com.harshsharma.telstra.assignment.models.commons.EventObject;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

// This activity act as a entry point and Controller for Fragments inside
public class DashBoardActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextViewCustom tvToolBarTitle;

    private ActionBar actionBar;
    private Toolbar toolbar;

    private BaseFragment baseFragment;
    private FragmentManager fragmentManager;

    @Override
    protected int getLayout() {
        return R.layout.activity_dash_board;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
        if (savedInstanceState == null)
            putFragmentInContainer(savedInstanceState);
    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        actionBar.setTitle(getString(R.string.app_name));
    }

    private void putFragmentInContainer(Bundle savedInstanceState) {
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        baseFragment = new FactsFragment();
        fragmentTransaction.add(R.id.dash_board_content, baseFragment, baseFragment.getFragmentName());
        fragmentTransaction.addToBackStack(baseFragment.getFragmentName());
        fragmentTransaction.commit();
    }


    @Subscribe
    @Override
    public void onEvent(final EventObject eventObject) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (eventObject.getId()) {
                    case EventCenter.NO_INTERNET_CONNECTION:
                        break;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        fragmentManager = getSupportFragmentManager();
        int backStackCount = fragmentManager.getBackStackEntryCount();
        if (backStackCount <= 1) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.exit)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            fragmentManager.popBackStackImmediate();
        }
    }

    public void setToolBarTitle(String title) {
        tvToolBarTitle.setText(title);
    }

}
