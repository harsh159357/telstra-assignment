package com.harshsharma.telstra.assignment.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.harshsharma.telstra.assignment.R;
import com.harshsharma.telstra.assignment.customview.TextViewCustom;
import com.harshsharma.telstra.assignment.models.commons.EventObject;
import com.harshsharma.telstra.assignment.utils.AppUtil;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

// Main Activity which will be launched at the start of the App
public class SplashActivity extends BaseActivity {

    @BindView(R.id.iv_splash)
    ImageView imageViewSplash;

    @BindView(R.id.splash_image)
    ImageView mSplashImage;

    @BindView(R.id.app_name)
    TextViewCustom mAppName;

    private Animation mAnimation;

    private Handler splashHandler;
    private Runnable splashRunnable = new Runnable() {
        @Override
        public void run() {
            launchNextActivity();
        }
    };

    @Override
    protected int getLayout() {
        return R.layout.activity_splash;
    }

    @Subscribe
    @Override
    public void onEvent(EventObject eventObject) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        doAnimation();
    }

    private void doAnimation() {
        mAnimation = AnimationUtils
                .loadAnimation(this, AppUtil.getRandomSplashAnimation());
        mAnimation.setDuration(SPLASH_ANIM_DURATION_MILLIS);
        mAppName.startAnimation(mAnimation);
        mSplashImage.startAnimation(mAnimation);
        splashHandler = new Handler();
        splashHandler.postDelayed(splashRunnable, DELAY_MILLIS);
    }

    @Override
    public boolean onTouchEvent(MotionEvent evt) {
        if (evt.getAction() == MotionEvent.ACTION_DOWN) {
            splashHandler.removeCallbacks(splashRunnable);
            launchNextActivity();
        }
        return true;
    }

    private void launchNextActivity() {
        try {
            ActivityCompat.finishAffinity(SplashActivity.this);
            Intent intent = new Intent();
            intent.setClass(SplashActivity.this, DashBoardActivity.class);
            startActivity(intent);
        } catch (Exception ignored) {
        }
    }

    @OnClick(R.id.splash_image)
    public void splashClick() {
        mAppName.startAnimation(mAnimation);
        mSplashImage.startAnimation(mAnimation);
    }

}
