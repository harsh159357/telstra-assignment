package com.harshsharma.telstra.assignment.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.harshsharma.telstra.assignment.R;
import com.harshsharma.telstra.assignment.customview.TextViewCustom;
import com.harshsharma.telstra.assignment.interfaces.AppConstants;
import com.harshsharma.telstra.assignment.interfaces.adapters.RowClickListener;
import com.harshsharma.telstra.assignment.models.Row;

import java.util.ArrayList;

public class RowAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements AppConstants {

    private ArrayList<Row> rowArrayList;
    private Context context;
    private RowClickListener chaptersClickListener;

    public RowAdapter(ArrayList<Row> rowArrayList,
                      Context context,
                      RowClickListener rowClickListener) {
        this.rowArrayList = rowArrayList;
        this.context = context;
        this.chaptersClickListener = rowClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View chaptersView
                = inflater.inflate(R.layout.item_row, viewGroup, false);
        viewHolder = new RowViewHolder(chaptersView);
        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        return -1;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        RowViewHolder rowViewHolder = (RowViewHolder) viewHolder;
        configureChaptersViewHolder(rowViewHolder, position);
    }

    private void configureChaptersViewHolder(RowViewHolder rowViewHolder, int position) {
        Row currentRow = rowArrayList.get(position);

        rowViewHolder.tvTitle.setText(currentRow.getTitle());

        rowViewHolder.tvDescription.setText(currentRow.getDescription());

        if (currentRow.getDescription() != null &&
                !currentRow.getDescription().equalsIgnoreCase("")) {
            rowViewHolder.tvDescription.setText(currentRow.getDescription());
        } else {
            rowViewHolder.tvDescription.setText(context.getResources().getString(R.string.details_not_available));
        }
        loadImage(currentRow.getImageHref(), rowViewHolder.networkImageView);
    }

    @Override
    public int getItemCount() {
        return rowArrayList.size();
    }

    private class RowViewHolder
            extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextViewCustom tvTitle, tvDescription;
        CardView rootLayout;
        ImageView networkImageView;

        RowViewHolder(View view) {
            super(view);
            networkImageView = view.findViewById(R.id.networkImageView);
            tvTitle = view.findViewById(R.id.tv_title);
            tvDescription = view.findViewById(R.id.tv_description);
            rootLayout = view.findViewById(R.id.root_row);
            rootLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Row row = rowArrayList.get(getAdapterPosition());
            switch (view.getId()) {
                case R.id.root_row:
                    chaptersClickListener.onRowClick(row, getAdapterPosition());
                    break;
            }
        }
    }

    private void loadImage(String imageUrl, ImageView imageView) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_image_place_holder);
        int width = context.getResources().getDimensionPixelOffset(R.dimen.dp200);
        int height = context.getResources().getDimensionPixelOffset(R.dimen.dp80);
        requestOptions.override(width, height);
        Glide.with(context).load(imageUrl).apply(requestOptions).into(imageView);

    }
}