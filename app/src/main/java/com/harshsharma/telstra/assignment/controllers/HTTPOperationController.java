package com.harshsharma.telstra.assignment.controllers;

import com.harshsharma.telstra.assignment.TelstraAssignmentApplication;
import com.harshsharma.telstra.assignment.interfaces.AppConstants;
import com.harshsharma.telstra.assignment.interfaces.retrofit.ApiInterface;
import com.harshsharma.telstra.assignment.webservice.ApiResponse;
import com.harshsharma.telstra.assignment.webservice.ConnectionUtil;

//Controller for Retrofit API interfacing
public class HTTPOperationController implements AppConstants {

    private static ApiInterface getApiInterface() {
        return TelstraAssignmentApplication.getInstance().getApiInterface();
    }

    public static ApiResponse getFacts() {
        return ConnectionUtil.execute(getApiInterface().getFacts(ApiEndPoint.FACTS_END_POINT));
    }
}