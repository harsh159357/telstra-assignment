package com.harshsharma.telstra.assignment.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.harshsharma.telstra.assignment.activities.BaseActivity;
import com.harshsharma.telstra.assignment.interfaces.AppConstants;


//All fragments in the app must extend this Fragment
public abstract class BaseFragment extends Fragment implements AppConstants {

    protected Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public abstract String getFragmentName();

    public void showToast(String toastMessage) {
        ((BaseActivity) context).showToast(toastMessage);
    }

    public void showProgress(String progressMessage) {
        ((BaseActivity) context).showProgress(progressMessage);
    }

    public void dismissProgress() {
        ((BaseActivity) context).dismissProgress();
    }

}
