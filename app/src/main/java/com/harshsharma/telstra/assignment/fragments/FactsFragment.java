package com.harshsharma.telstra.assignment.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.harshsharma.telstra.assignment.R;
import com.harshsharma.telstra.assignment.activities.DashBoardActivity;
import com.harshsharma.telstra.assignment.adapters.RowAdapter;
import com.harshsharma.telstra.assignment.interfaces.adapters.RowClickListener;
import com.harshsharma.telstra.assignment.models.Facts;
import com.harshsharma.telstra.assignment.models.Row;
import com.harshsharma.telstra.assignment.models.commons.EventObject;
import com.harshsharma.telstra.assignment.requesters.GetFactsRequester;
import com.harshsharma.telstra.assignment.utils.BackgroundExecutor;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FactsFragment extends BaseFragment implements RowClickListener {
    private Facts facts = new Facts();
    private ArrayList<Row> rowArrayList = new ArrayList<>();

    @BindView(R.id.rv_facts)
    RecyclerView recyclerViewFacts;
    RowAdapter rowAdapter;

    @BindView(R.id.empty_container)
    LinearLayout emptyContainer;

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private View rootView;
    private Unbinder unbinder;

    private DashBoardActivity dashBoardActivityContext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            rowArrayList = savedInstanceState.getParcelableArrayList(BundleOrParcelableExtras.ROWS);
            facts.setRows(rowArrayList);
            facts.setTitle(savedInstanceState.getString(BundleOrParcelableExtras.TITLE));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = getView() != null ? getView() :
                inflater.inflate(R.layout.fragment_facts, container, false);
        dashBoardActivityContext = (DashBoardActivity) context;
        EventBus.getDefault().register(this);
        unbinder = ButterKnife.bind(this, rootView);

        dashBoardActivityContext.setToolBarTitle(dashBoardActivityContext.getString(R.string.facts));

        setHasOptionsMenu(true);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(swipeRefreshListener);
        initUploadedImageAdapter(savedInstanceState);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public String getFragmentName() {
        return FactsFragment.class.getSimpleName();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        EventBus.getDefault().unregister(this);
    }


    SwipeRefreshLayout.OnRefreshListener swipeRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            performSwipeRefresh();
        }
    };

    void performSwipeRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        loadFacts();
    }

    void initUploadedImageAdapter(Bundle bundle) {
        recyclerViewFacts.setLayoutManager(new LinearLayoutManager(dashBoardActivityContext));
        if (bundle != null) {
            if (!rowArrayList.isEmpty()) {
                emptyContainer.setVisibility(View.GONE);
            }
            rowAdapter = new RowAdapter(rowArrayList, dashBoardActivityContext, this);
            recyclerViewFacts.setAdapter(rowAdapter);
            if (facts.getTitle() != null) {
                dashBoardActivityContext.setToolBarTitle(facts.getTitle());
            } else {
                dashBoardActivityContext.setToolBarTitle(dashBoardActivityContext.getString(R.string.facts));
            }
        } else {
            rowAdapter = new RowAdapter(rowArrayList, dashBoardActivityContext, this);
            recyclerViewFacts.setAdapter(rowAdapter);
            fetchAllFacts();
        }
    }

    private void fetchAllFacts() {
        rowArrayList.clear();
        showProgress(getString(R.string.progress_text_loading));
        loadFacts();
    }

    @Subscribe
    public void onEvent(final EventObject eventObject) {
        dashBoardActivityContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (eventObject.getId()) {
                    case EventCenter.GET_FACTS_SUCCESSFUL:
                        facts = (Facts) eventObject.getObject();
                        dashBoardActivityContext.setToolBarTitle(facts.getTitle());
                        emptyContainer.setVisibility(View.GONE);
                        rowArrayList.clear();
                        rowArrayList.addAll(facts.getRows());
                        rowAdapter.notifyDataSetChanged();
                        showToast(getString(R.string.toast_text_get_facts_successfully));
                        swipeRefreshLayout.setRefreshing(false);
                        dismissProgress();
                        break;
                    case EventCenter.GET_FACTS_UNSUCCESSFUL:
                        commonOperation();
                        showToast(getString(R.string.toast_text_unable_to_get_facts));
                        break;
                    case EventCenter.NO_INTERNET_CONNECTION:
                        commonOperation();
                        showToast(getString(R.string.toast_text_no_internet_connection));
                        break;
                }
            }
        });
    }

    void commonOperation() {
        facts = new Facts();
        rowArrayList.clear();
        rowAdapter.notifyDataSetChanged();
        emptyContainer.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(false);
        dismissProgress();
        dashBoardActivityContext.setToolBarTitle(dashBoardActivityContext.getString(R.string.facts));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_facts, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_refresh) {
            performSwipeRefresh();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(BundleOrParcelableExtras.TITLE, facts.getTitle());
        outState.putParcelableArrayList(BundleOrParcelableExtras.ROWS, facts.getRows());
    }

    private void loadFacts() {
        BackgroundExecutor.getInstance().execute(new GetFactsRequester());
    }

    @Override
    public void onRowClick(Row row, int position) {
        showToast(row.getTitle());
    }

}