package com.harshsharma.telstra.assignment.interfaces;

import com.harshsharma.telstra.assignment.R;

// All common constants can be accessed from here
public interface AppConstants {

    //Splash Delay
    int DELAY_MILLIS = 1600;

    //Splash Animation Duration
    int SPLASH_ANIM_DURATION_MILLIS = 1300;

    //Some sample splash animations
    int[] splashAnimation = new int[]{
            R.anim.fade_in,
            R.anim.zoom_in,
            R.anim.slide_down,
            R.anim.bounce_down,
            R.anim.rotate_clockwise,
            R.anim.rotate_anti_clockwise,
    };

    interface EventCenter {
        int NO_INTERNET_CONNECTION = 0;
        int GET_FACTS_SUCCESSFUL = 500;
        int GET_FACTS_UNSUCCESSFUL = 501;
    }

    //API Urls
    interface ApiEndPoint {
        String BASE_URL = "https://dl.dropboxusercontent.com/";
        String FACTS_JSON = "s/2iodh4vg0eortkl/facts.json";

        String FACTS_END_POINT = BASE_URL + FACTS_JSON;
    }


    interface BundleOrParcelableExtras {
        String PACKAGE_NAME = "com.harshsharma.telstra.assignment/";

        String TITLE = PACKAGE_NAME + "TITLE";

        String ROWS = PACKAGE_NAME + "ROWS";

    }
}
