package com.harshsharma.telstra.assignment.interfaces.adapters;

import com.harshsharma.telstra.assignment.models.Row;

public interface RowClickListener {
    void onRowClick(Row row, int position);
}
