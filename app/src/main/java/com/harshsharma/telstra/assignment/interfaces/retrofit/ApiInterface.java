package com.harshsharma.telstra.assignment.interfaces.retrofit;

import com.harshsharma.telstra.assignment.models.Facts;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

// Api Interface Used for Api Integration using Retrofit
public interface ApiInterface {
    @GET
    Call<Facts> getFacts(@Url String url);
}
