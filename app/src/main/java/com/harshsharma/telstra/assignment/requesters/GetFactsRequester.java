package com.harshsharma.telstra.assignment.requesters;

import com.harshsharma.telstra.assignment.TelstraAssignmentApplication;
import com.harshsharma.telstra.assignment.controllers.HTTPOperationController;
import com.harshsharma.telstra.assignment.interfaces.BaseRequester;
import com.harshsharma.telstra.assignment.models.Facts;
import com.harshsharma.telstra.assignment.models.commons.EventObject;
import com.harshsharma.telstra.assignment.utils.AppUtil;
import com.harshsharma.telstra.assignment.webservice.ApiResponse;

import org.greenrobot.eventbus.EventBus;

public class GetFactsRequester implements BaseRequester {

    @Override
    public void run() {
        if (AppUtil.isConnected(TelstraAssignmentApplication.getInstance())) {
            ApiResponse<Facts> factsApiResponse = HTTPOperationController.getFacts();

            if (factsApiResponse.getResponse() != null) {
                EventBus.getDefault().post(new EventObject(EventCenter.GET_FACTS_SUCCESSFUL, factsApiResponse.getResponse()));
            } else {
                EventBus.getDefault().post(new EventObject(EventCenter.GET_FACTS_UNSUCCESSFUL, null));
            }
        } else {
            EventBus.getDefault().post(new EventObject(EventCenter.NO_INTERNET_CONNECTION, null));
        }
    }
}
