package com.harshsharma.telstra.assignment.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.harshsharma.telstra.assignment.interfaces.AppConstants;

import java.security.SecureRandom;
import java.util.Date;

/*
        Common task done in app must be done here so they can be used anywhere in this app
        without writing code again and again(Minimize Redundancy).
*/
public class AppUtil implements AppConstants {
    private static final String TAG = AppUtil.class.getName();

    public static int generateRandomInteger(int min, int max) {
        SecureRandom rand = new SecureRandom();
        rand.setSeed(new Date().getTime());
        return rand.nextInt((max - min) + 1) + min;
    }

    public static int getRandomSplashAnimation() {
        return AppConstants.splashAnimation[generateRandomInteger(0, splashAnimation.length - 1)];
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) return false;
        NetworkInfo.State network = info.getState();
        return (network == NetworkInfo.State.CONNECTED || network == NetworkInfo.State.CONNECTING);
    }

}
