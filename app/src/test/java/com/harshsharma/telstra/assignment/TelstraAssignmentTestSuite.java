package com.harshsharma.telstra.assignment;

import com.harshsharma.telstra.assignment.api.FactsApiTest;
import com.harshsharma.telstra.assignment.model.FactsTest;
import com.harshsharma.telstra.assignment.model.RowTest;
import com.harshsharma.telstra.assignment.utils.AppUtilTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({

        FactsApiTest.class,
        FactsTest.class,
        RowTest.class,
        AppUtilTest.class

})

public class TelstraAssignmentTestSuite {
}  	