package com.harshsharma.telstra.assignment.api;

import com.harshsharma.telstra.assignment.TestUtil;
import com.harshsharma.telstra.assignment.interfaces.AppConstants;
import com.harshsharma.telstra.assignment.interfaces.retrofit.ApiInterface;
import com.harshsharma.telstra.assignment.models.Facts;
import com.harshsharma.telstra.assignment.webservice.ApiResponse;
import com.harshsharma.telstra.assignment.webservice.ConnectionUtil;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FactsApiTest implements AppConstants {
    private ApiInterface apiInterface;

    @Before
    public void beforeTest() {
        // Initializing Api Interface
        apiInterface = TestUtil.getApiInterface();
    }


    @Test
    public void getFactsSuccessfulTest() throws InterruptedException {
        ApiResponse apiResponse = ConnectionUtil.execute(apiInterface.getFacts(ApiEndPoint.FACTS_JSON));
        Facts facts = (Facts) apiResponse.getResponse();

        Assert.assertNotNull(facts);
        Assert.assertTrue("Title is there", facts.getTitle().length() > 0);
        Assert.assertTrue("Rows are there", (facts.getRows().size() > 0));
    }

    @Test
    public void getFactsUnSuccessfulTest() throws InterruptedException {
        ApiResponse apiResponse = ConnectionUtil.execute(apiInterface.getFacts(ApiEndPoint.BASE_URL));
        Facts facts = (Facts) apiResponse.getResponse();

        Assert.assertNull("Facts are not loaded", facts);
    }

    @After
    public void afterTest() {
        apiInterface = null;
    }

}