package com.harshsharma.telstra.assignment.model;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.harshsharma.telstra.assignment.TestUtil;
import com.harshsharma.telstra.assignment.models.Facts;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.InputStream;

import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class FactsTest {
    private Facts facts;
    private String json;

    @Mocked
    Context mMockContext;
    @Mocked
    AssetManager assetManager;

    private InputStream productListInputStream;

    @Before
    public void setUp() throws Exception {
        readFactsJson(TestUtil.FACTS_BEST_CASE_JSON);
    }

    @After
    public void tearDown() {
        json = null;
    }

    @Test
    public void testTitleNotNull() {
        Assert.assertNotNull(facts.getTitle());
    }

    @Test
    public void testTitleCheck() {
        Assert.assertEquals("About Canada", facts.getTitle());
        Assert.assertNotEquals("AboutCanada", facts.getTitle());
    }

    @Test
    public void testRowsIsEmpty() {
        Assert.assertFalse(facts.getRows().isEmpty());
    }

    @Test
    public void testRowsCount() {
        Assert.assertEquals(9, facts.getRows().size());
        Assert.assertNotEquals(15, facts.getRows().size());
    }

    private void readFactsJson(final String resourceFileName) throws Exception {
        productListInputStream = getClass().getClassLoader().getResourceAsStream(resourceFileName);
        new NonStrictExpectations() {
            {
                mMockContext.getAssets();
                returns(assetManager);

                assetManager.open(resourceFileName);
                returns(productListInputStream);
            }
        };
        json = TestUtil.loadJSONFromAssets(resourceFileName, mMockContext);
        facts = new Gson().fromJson(json, Facts.class);
    }

}