package com.harshsharma.telstra.assignment.model;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.harshsharma.telstra.assignment.TestUtil;
import com.harshsharma.telstra.assignment.models.Facts;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

import mockit.Mocked;
import mockit.NonStrictExpectations;

public class RowTest {
    private Facts facts;
    private String json;

    @Mocked
    Context mMockContext;
    @Mocked
    AssetManager assetManager;

    private InputStream productListInputStream;

    @Before
    public void setUp() throws Exception {
        readFactsJson(TestUtil.WORST_CASE_JSON);
    }

    @After
    public void tearDown() {
        json = null;
    }

    @Test
    public void testTitle() {
        Assert.assertEquals("Geography", facts.getRows().get(3).getTitle());
    }

    @Test
    public void testTitleNotNull() {
        Assert.assertNotNull(facts.getRows().get(0).getTitle());
    }

    @Test
    public void getDescription() {
        Assert.assertEquals("Nous parlons tous les langues importants.", facts.getRows().get(4).getDescription());
    }

    @Test
    public void getDescriptionNotNull() {
        Assert.assertNotNull(facts.getRows().get(1).getDescription());
    }

    @Test
    public void testImageHref() {
        Assert.assertEquals("http://images.findicons.com/files/icons/662/world_flag/128/flag_of_canada.png", facts.getRows().get(0).getImageHref());
    }

    @Test
    public void getImageHrefNull() {
        Assert.assertNull(facts.getRows().get(3).getImageHref());
    }

    private void readFactsJson(final String resourceFileName) throws Exception {
        productListInputStream = getClass().getClassLoader().getResourceAsStream(resourceFileName);
        new NonStrictExpectations() {
            {
                mMockContext.getAssets();
                returns(assetManager);

                assetManager.open(resourceFileName);
                returns(productListInputStream);
            }
        };
        json = TestUtil.loadJSONFromAssets(resourceFileName, mMockContext);
        facts = new Gson().fromJson(json, Facts.class);
    }

}