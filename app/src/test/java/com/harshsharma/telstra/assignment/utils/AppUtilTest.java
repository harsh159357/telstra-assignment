package com.harshsharma.telstra.assignment.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.junit.Assert;
import org.junit.Test;

import mockit.Mocked;
import mockit.NonStrictExpectations;

public class AppUtilTest {
    @Mocked
    Context mMockContext;
    @Mocked
    NetworkInfo networkInfo;
    @Mocked
    ConnectivityManager connectivityManager;

    @Test
    public void isConnectionAvailable() {
        new NonStrictExpectations() {
            {
                mMockContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                returns(connectivityManager);

                connectivityManager.getActiveNetworkInfo();
                returns(networkInfo);

                networkInfo.getState();
                returns(NetworkInfo.State.CONNECTED);
            }
        };
        Assert.assertTrue(AppUtil.isConnected(mMockContext));
    }

    @Test
    public void isConnectionNotAvailable() {
        new NonStrictExpectations() {
            {
                mMockContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                returns(connectivityManager);

                connectivityManager.getActiveNetworkInfo();
                returns(networkInfo);

                networkInfo.getState();
                returns(NetworkInfo.State.DISCONNECTED);
            }
        };
        Assert.assertFalse(AppUtil.isConnected(mMockContext));
    }

}