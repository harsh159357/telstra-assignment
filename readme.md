# Telstra Assignment

## [Telstra Assignment in Action](screenshots/assignment_in_action.mp4)

## [Telstra Assignment Apk](https://drive.google.com/file/d/1z8Vq-x03BFei6MAH2GaSq4rWVPqHPT3U/view?usp=sharing)


[![API](https://img.shields.io/badge/API-15%2B-blue.svg?style=flat)](https://android-arsenal.com/api?level=15)


## Overview of Assignment
* Will show the feed from [link](https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json)
* Images are loaded Lazily using Glide
* ButterKnife is used for view injection.
* EventBus for Publishing and Subscribing of Custom Events in App. 
* Random Splash Animation Before going to DashBoard using xml files.
* Retrofit is Used for the Rest API integration.
* Swipe to Down to Refresh and Manual Refresh.
* Landscape and Portrait Mode Support.
* Ripple Effect on Item Touch
* Android UI Test Cases and Unit Test Cases are covered.
* Bitbucket and Git are used for VCS.

## Libraries Used

[Retrofit](https://square.github.io/retrofit/)  [Glide](https://github.com/bumptech/glide) 
[Event Bus](https://github.com/greenrobot/EventBus)  [Butterknife](https://jakewharton.github.io/butterknife/) 


### Assignment By
# [Harsh Sharma](http://bit.ly/githarsh)

Technology Analyst Infosys

[StackOverFlow](http://bit.ly/stackharsh)
[LinkedIn](http://bit.ly/lnkdharsh)
[Facebook](http://bit.ly/harshfb)
